-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema internfinder
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema internfinder
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `internfinder` DEFAULT CHARACTER SET utf8 ;
USE `internfinder` ;

-- -----------------------------------------------------
-- Table `internfinder`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`categories` (
  `idCategorie` INT(11) NOT NULL AUTO_INCREMENT,
  `categorie_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategorie`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`users` (
  `user_id` INT(11) NOT NULL,
  `user_type` VARCHAR(45) NOT NULL,
  `mail` VARCHAR(250) NULL,
  `phone_number` VARCHAR(45) NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`companies` (
  `user_id` INT(11) NOT NULL,
  `company_name` VARCHAR(45) NOT NULL,
  `company_address` VARCHAR(45) NOT NULL,
  INDEX `fk_company_user_id_idx` (`user_id` ASC),
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_company_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `internfinder`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`tags` (
  `idTags` INT(11) NOT NULL AUTO_INCREMENT,
  `tag_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idTags`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`programmes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`programmes` (
  `id_programme` INT NOT NULL,
  `nom_programme` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_programme`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `internfinder`.`annonces`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`annonces` (
  `id_annonce` INT(11) NOT NULL AUTO_INCREMENT,
  `id_annonceur` INT(11) NOT NULL,
  `stage_title` VARCHAR(45) NOT NULL,
  `stage_description` VARCHAR(1000) NOT NULL,
  `program_id` INT(11) NULL,
  `start_date_stage` DATE NOT NULL,
  `end_date_stage` DATE NOT NULL,
  `compensation` TINYINT(1) NOT NULL,
  `idTags` INT(11) NULL,
  `bool_college` TINYINT(1) NULL DEFAULT '0',
  `job_title` INT NOT NULL,
  `date_publication` DATE NOT NULL,
  PRIMARY KEY (`id_annonce`),
  INDEX `fk_annonceur_stage_id_idx` (`id_annonceur` ASC),
  INDEX `fk_program_description_id_idx` (`program_id` ASC),
  CONSTRAINT `fk_annonceur_stage_id`
    FOREIGN KEY (`id_annonceur`)
    REFERENCES `internfinder`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_program_description_id`
    FOREIGN KEY (`program_id`)
    REFERENCES `internfinder`.`categories` (`idCategorie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_tag_id`
    FOREIGN KEY (`id_annonce`)
    REFERENCES `internfinder`.`tags` (`idTags`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_programme_id`
    FOREIGN KEY (`program_id`)
    REFERENCES `internfinder`.`programmes` (`id_programme`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`messages` (
  `id_Message` INT(11) NOT NULL AUTO_INCREMENT,
  `id_expediteur` INT(11) NOT NULL,
  `id_destinataire` INT(11) NOT NULL,
  `date_message` DATE NOT NULL,
  `corps_message` VARCHAR(500) NOT NULL,
  `titre_message` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_Message`),
  INDEX `fk_id_destinataire_user_idx` (`id_destinataire` ASC),
  INDEX `fk_id_expediteur_user_idx` (`id_expediteur` ASC),
  CONSTRAINT `fk_id_destinataire_user`
    FOREIGN KEY (`id_destinataire`)
    REFERENCES `internfinder`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_expediteur_user`
    FOREIGN KEY (`id_expediteur`)
    REFERENCES `internfinder`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`students` (
  `da_student` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `age` INT(11) NOT NULL,
  `college_id` INT(11) NOT NULL,
  `program_id` INT(11) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `superviser_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`da_student`),
  INDEX `fk_Student_User_idx` (`user_id` ASC),
  INDEX `fk_programme_user_idx` (`program_id` ASC),
  CONSTRAINT `fk_Student_User`
    FOREIGN KEY (`user_id`)
    REFERENCES `internfinder`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_programme_user`
    FOREIGN KEY (`program_id`)
    REFERENCES `internfinder`.`categories` (`idCategorie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`teachers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`teachers` (
  `user_id` INT(11) NOT NULL,
  `college_id` INT(11) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  INDEX `fk_Teacher_User1_idx` (`user_id` ASC),
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_Teacher_User`
    FOREIGN KEY (`user_id`)
    REFERENCES `internfinder`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `internfinder`.`supervisings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `internfinder`.`supervisings` (
  `id_teacher` INT(11) NOT NULL,
  `id_student` INT(11) NULL DEFAULT NULL,
  `id_supervising` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_teacher`),
  INDEX `fk_supervising_student_id_idx` (`id_student` ASC),
  CONSTRAINT `fk_supervising_student_id`
    FOREIGN KEY (`id_student`)
    REFERENCES `internfinder`.`students` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_supervising_teacher_id`
    FOREIGN KEY (`id_teacher`)
    REFERENCES `internfinder`.`teachers` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
