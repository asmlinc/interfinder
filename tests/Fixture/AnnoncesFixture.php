<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AnnoncesFixture
 *
 */
class AnnoncesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id_annonce' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'id_annonceur' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'stage_title' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'stage_description' => ['type' => 'string', 'length' => 1000, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'program_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'start_date_stage' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'end_date_stage' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'compensation' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'idTags' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bool_college' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'job_title' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date_publication' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_annonceur_stage_id_idx' => ['type' => 'index', 'columns' => ['id_annonceur'], 'length' => []],
            'fk_program_description_id_idx' => ['type' => 'index', 'columns' => ['program_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id_annonce'], 'length' => []],
            'fk_annonceur_stage_id' => ['type' => 'foreign', 'columns' => ['id_annonceur'], 'references' => ['users', 'user_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_programme_id' => ['type' => 'foreign', 'columns' => ['program_id'], 'references' => ['programmes', 'id_programme'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_program_description_id' => ['type' => 'foreign', 'columns' => ['program_id'], 'references' => ['categories', 'idCategorie'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_tags_tag_id' => ['type' => 'foreign', 'columns' => ['id_annonce'], 'references' => ['tags', 'idTags'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id_annonce' => 1,
            'id_annonceur' => 1,
            'stage_title' => 'Lorem ipsum dolor sit amet',
            'stage_description' => 'Lorem ipsum dolor sit amet',
            'program_id' => 1,
            'start_date_stage' => '2016-04-08',
            'end_date_stage' => '2016-04-08',
            'compensation' => 1,
            'idTags' => 1,
            'bool_college' => 1,
            'job_title' => 1,
            'date_publication' => '2016-04-08'
        ],
    ];
}
