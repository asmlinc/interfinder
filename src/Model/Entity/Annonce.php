<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Annonce Entity.
 *
 * @property int $id_annonce
 * @property int $id_annonceur
 * @property string $stage_title
 * @property string $stage_description
 * @property int $program_id
 * @property \App\Model\Entity\Programme $programme
 * @property \Cake\I18n\Time $start_date_stage
 * @property \Cake\I18n\Time $end_date_stage
 * @property bool $compensation
 * @property int $idTags
 * @property bool $bool_college
 * @property int $job_title
 * @property \Cake\I18n\Time $date_publication
 */
class Annonce extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id_annonce' => false,
    ];
}
