<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity.
 *
 * @property int $id_Message * @property int $id_expediteur * @property int $id_destinataire * @property \App\Model\Entity\User $user * @property \Cake\I18n\Time $date_message * @property string $corps_message * @property string $titre_message * @property bool $isRead */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id_Message' => false,
    ];
}
