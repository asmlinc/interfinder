<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity.
 *
 * @property int $user_id * @property \App\Model\Entity\User $user * @property int $da_student * @property int $age * @property int $college_id * @property \App\Model\Entity\College $college * @property int $program_id * @property \App\Model\Entity\Category $category * @property string $first_name * @property string $last_name * @property \App\Model\Entity\Superviser $superviser */
class Student extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
