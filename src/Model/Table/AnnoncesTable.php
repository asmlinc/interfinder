<?php
namespace App\Model\Table;

use App\Model\Entity\Annonce;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Annonces Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Programmes
 */
class AnnoncesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('annonces');
        $this->displayField('id_annonce');
        $this->primaryKey('id_annonce');

        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'id_annonceur',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_annonce')
            ->allowEmpty('id_annonce', 'create');

        $validator
            ->integer('id_annonceur')
            ->requirePresence('id_annonceur', 'create')
            ->notEmpty('id_annonceur');

        $validator
            ->requirePresence('stage_title', 'create')
            ->notEmpty('stage_title');

        $validator
            ->requirePresence('stage_description', 'create')
            ->notEmpty('stage_description');

        $validator
            ->date('start_date_stage')
            ->requirePresence('start_date_stage', 'create')
            ->notEmpty('start_date_stage');

        $validator
            ->date('end_date_stage')
            ->requirePresence('end_date_stage', 'create')
            ->notEmpty('end_date_stage');

        $validator
            ->boolean('compensation')
            ->requirePresence('compensation', 'create')
            ->notEmpty('compensation');

        $validator
            ->boolean('bool_college')
            ->allowEmpty('bool_college');

        $validator
            ->allowEmpty('job_title');

        $validator
            ->date('date_publication')
            ->requirePresence('date_publication', 'create')
            ->notEmpty('date_publication');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['program_id'], 'Programs'));
        return $rules;
    }
}
