<?php
namespace App\Model\Table;

use App\Model\Entity\Message;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Messages Model
 *
 */
class MessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('messages');
        $this->displayField('id_Message');
        $this->primaryKey('id_Message');

        $this->belongsTo('Users', [
            'foreignKey' => 'id_expediteur',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'id_destinataire',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_Message')            ->allowEmpty('id_Message', 'create');
        $validator
            ->integer('id_expediteur')            ->requirePresence('id_expediteur', 'create')            ->notEmpty('id_expediteur');
        $validator
            ->integer('id_destinataire')            ->requirePresence('id_destinataire', 'create')            ->notEmpty('id_destinataire');
        $validator
            ->date('date_message')            ->requirePresence('date_message', 'create')            ->notEmpty('date_message');
        $validator
            ->requirePresence('corps_message', 'create')            ->notEmpty('corps_message');
        $validator
            ->requirePresence('titre_message', 'create')            ->notEmpty('titre_message');
        $validator
            ->boolean('isRead')            ->requirePresence('isRead', 'create')            ->notEmpty('isRead');
        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['id_expediteur'], 'Users'));
        $rules->add($rules->existsIn(['id_destinataire'], 'Users'));
        return $rules;
    }
}
