<?= $this->Html->css('messages.css') ?>


<div class="messages index large-9 medium-8 columns content">
	<h1 class="title"><?= __('Boite de réception') ?></h1>
	<div class="col-md-12 no-padding" id="actions-sidebar">
		<div class="col-md-2 col-xs-12"></div>
		<div class="col-md-8 col-xs-12 no-padding">
			<button
				class="btn btn-primary btn-lg btn-custom msg-button"><?= $this->Html->link(__('Nouveau message'), ['action' => 'add']) ?></button>
		</div>
		<div class="col-md-2 col-xs-12"></div>
	</div>
	<div class="col-md-2 col-xs-12"></div>
	<div class="table-responsive table-inbox col-md-8 col-xs-12">
		<table class="table" cellpadding="0" cellspacing="0">
			<thead>
			<tr>
				<th><?= $this->Paginator->sort('Expéditeur') ?></th>
				<th><?= $this->Paginator->sort('Destinataire') ?></th>
				<th><?= $this->Paginator->sort('Date') ?></th>
				<th><?= $this->Paginator->sort('Titre') ?></th>
				<th><?= $this->Paginator->sort('') ?></th>
				<th class="actions"></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($messages as $message): ?>
				<tr>
					<td><?= $message->has('sender') ? $this->Html->link($message->sender[0]->fullname, ['controller' => 'Users', 'action' => 'profil', $message->sender[0]->user_id]) : '' ?></td>
					<td><?= $message->has('receiver') ? $this->Html->link($message->receiver[0]->fullname, ['controller' => 'Users', 'action' => 'profil', $message->receiver[0]->user_id]) : '' ?></td>
					<td><?= h($message->date_message) ?></td>
					<td><?= h($message->titre_message) ?></td>
					<td><?= h($message->isRead ? __('Lu') : __('Non Lu')) ?></td>
					<td class="actions">
						<?= $this->Html->link(__('Voir'), ['action' => 'view', $message->id_Message], ['class' => 'view-msg']) ?>
						<?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $message->id_Message], ['class' => 'delete-msg'], ['confirm' => __('Etes vous sûre de vouloir supprimer ce message?', $message->id_Message)]) ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<div class="paginator">
			<div class="col-md-2 col-xs-12"></div>
			<div class="col-md-8 col-xs-12 center-content">
				<ul class="pagination">
					<?= $this->Paginator->prev('< ' . __('Précedent')) ?>
					<?= $this->Paginator->numbers() ?>
					<?= $this->Paginator->next(__('Suivant') . ' >') ?>
				</ul>
			</div>
			<div class="col-md-2 col-xs-12"></div>
		</div>
	</div>
	<div class="col-md-2 col-xs-12"></div>
</div>
