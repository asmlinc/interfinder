<?= $this->Html->css('messages.css') ?>
<div>
	<div class="col-md-12 no-padding" id="actions-sidebar">
		<div class="col-md-3 col-xs-12"></div>
		<div class="col-md-6  col-xs-12 no-padding">
			<button
				class="btn btn-primary btn-lg btn-custom msg-button"><?= $this->Html->link(__('Boite Recéption'), ['action' => 'index']) ?></button>
		</div>
		<div class="col-md-3 col-xs-12"></div>
	</div>
	<div class="col-md-12 no-padding">
		<div class="col-md-3 col-xs-12"></div>
		<div class="col-md-6 col-xs-12 new-msg-form no-padding">
		<?= $this->Form->create($message) ?>
		<fieldset>
			<legend class = "title"><?= __('Nouveau message') ?></legend>
			<?php
			echo $this->Form->input('id_destinataire', ['options' => $users, 'label' => 'Destinataire']);
			echo $this->Form->input('titre_message', ['label' => 'Titre du message']);
			echo $this->Form->input('corps_message', ['label' => '', 'class' => 'msg-body']);;
			?>
		</fieldset>
		<?= $this->Form->button(__('Envoyer'), ['class' => 'btn btn-primary btn-lg btn-custom msg-button']) ?>
		<?= $this->Form->end() ?>
			</div>
		<div class="col-md-3 col-xs-12"></div>
	</div>

</div>
