<?= $this->Html->css('messages.css') ?>

<div class="col-md-12 no-padding" id="actions-sidebar">
	<div class="col-md-2 col-xs-12"></div>
	<div class="col-md-8  col-xs-12 no-padding msg-view-buttons">
		<button class="btn btn-primary btn-lg btn-custom msg-button">
			<?= $this->Form->postLink(__('Supprimer Message'), ['action' => 'delete', $message->id_Message], ['confirm' => __('Etes vous sûre de vouloir supprimer ce message?', $message->id_Message)]) ?>
		</button>
		<button class="btn btn-primary btn-lg btn-custom msg-button">
			<?= $this->Html->link(__('Nouveau Message'), ['action' => 'add']) ?>
		</button>
		<button class="btn btn-primary btn-lg btn-custom msg-button">
			<?= $this->Html->link(__('Boite Réception'), ['action' => 'index']) ?>
		</button>
	</div>
	<div class="col-md-2 col-xs-12"></div>
</div>
<div class="col-md-12 no-padding">
	<div class="col-md-2 col-xs-12"></div>
	<table class="vertical-table table-inbox table-msg col-md-8 col-xs-12 no-padding">
		<tr>
			<th><?= __('Expéditeur') ?></th>
			<td><?= $message->has('sender') ? $this->Html->link($message->sender[0]->fullname, ['controller' => 'Users', 'action' => 'profil', $message->sender[0]->username]) : '' ?></td>
		</tr>
		<tr>
			<th><?= __('Destinataire') ?></th>
			<td><?= $message->has('receiver') ? $this->Html->link($message->receiver[0]->fullname, ['controller' => 'Users', 'action' => 'profil', $message->receiver[0]->username]) : '' ?></td>
		</tr>
		<tr>
			<th><?= __('Titre du message') ?></th>
			<td><?= h($message->titre_message) ?></td>
		</tr>
		<tr>
			<th><?= __('Date du message') ?></th>
			<td><?= h($message->date_message) ?></td>
		</tr>
		<tr>
			<th><?= __('Message lu:') ?></th>
			<td><?= $message->isRead ? __('Oui') : __('Non'); ?></td>
		</tr>
	</table>
	<div class="col-md-2 col-xs-12"></div>
	<div class="col-md-12 no-padding msg-content">
		<div class="col-md-2 col-xs-12"></div>
		<div class="col-md-8 col-xs-12 table-inbox no-padding">
			<?= $this->Text->autoParagraph(h($message->corps_message)); ?>
		</div>
		<div class="col-md-2 col-xs-12"></div>
	</div>
</div>
