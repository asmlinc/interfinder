<!DOCTYPE html>
<html>
    <head>
      <title>
        Internfinder
      </title>
        <?= $this->Html->css('annonces.css') ?>
        <?= $this->Html->css('about.css') ?>
        <?= $this->Html->css('contact.css') ?>
        <?= $this->Html->script('contact.js') ?>
    </head>
    <body>
          <div class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="title">Nous contacter</div>
                <hr>
                <div class="sub-title desc">
                  Etiam feugiat sapien ac dui dictum pulvinar. Phasellus dapibus mi nec dui congue congue. In quis mauris ligula. Mauris nisl augue, molestie vitae viverra pharetra, pretium eu quam. Vivamus a facilisis tortor, a feugiat tellus. Fusce et massa in lorem accumsan dapibus nec sit amet nisl.
                </div>
              </div>

              <div class="col-md-3">
              </div>
              <div class="col-md-6">
                <div class="contact_content">
                    <?php echo $this->Form->input('', array('class'=>'forminput-new',"placeholder"=>"Entrez votre nom")); ?>
                    <br>
                    <?php echo $this->Form->input('', array('type' => 'textarea', "placeholder"=>"Entrez votre message..."));?>
                    <br>
                    <?php echo $this->Form->Submit(__('Envoyer'), array('class' => 'button_submit')); ?>
                </div>
              </div>
              <div class="col-md-3">
              </div>
            </div>
        </div>
        <div class="sent_message">
          <span>Votre message a bien été envoyé !</span>
          <h5>Nous vous répondrons dans les plus brefs délais. En attendant, n'hésitez pas à nous faire part de vos questions
          et recommendations</h5>
          <h6> - L'équipe Internfinder</h6>
        </div>
  </body>
</html>