<html>
    <head>
        <?= $this->Html->css('annonces.css') ?>
        <?= $this->Html->css('about.css') ?>
        <?= $this->Html->css('contact.css') ?>
        <?= $this->Html->script('about.js') ?>
    </head>
        <body>
	          <div class="title_about"> Notre équipe </div>
	          <hr>
			  <div class="sub-title desc">
				  Etiam feugiat sapien ac dui dictum pulvinar. Phasellus dapibus mi nec dui congue congue. In quis mauris ligula. Mauris nisl augue, molestie vitae viverra pharetra, pretium eu quam. Vivamus a facilisis tortor, a feugiat tellus. Fusce et massa in lorem accumsan dapibus nec sit amet nisl.
				  Etiam feugiat sapien ac dui dictum pulvinar. Phasellus dapibus mi nec dui congue congue. In quis mauris ligula. Mauris nisl augue, molestie vitae viverra pharetra, pretium eu quam. Vivamus a facilisis tortor, a feugiat tellus. Fusce et massa in lorem accumsan dapibus nec sit amet nisl.

			  </div>
	          <div class="team">
	          	<div class="col-md-3 teammate_desc t1">
	          		<div class="container">
	          			<img src="https://scontent-yyz1-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/1918340_10156465331900613_9196187076147731552_n.jpg?oh=9fa43d5e834d9f9b81b4fb350f1c2d64&oe=57ABD4EF">
	          			<span class="tag">Developpeur</span>
	          			<div class="member_name">
		          			<span class="name">Mélissa Sissoko</span>
		          			<hr>
	          			</div>
	          		</div>
	          	</div>



	          	<div class="col-md-3 teammate_desc t2">
	          		<div class="container">
	          			<img src="https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/10917444_1048931095124206_5684096769635220426_n.jpg?oh=69d55348cc1cb2dbc04d5c3da8f60e48&oe=57B92AF3">
	          			<span class="tag">Developpeur</span>
	          			<div class="member_name">
	          				<span class="name">Louis Contant</span>
	          			<hr>
	          		</div>
	          		</div>
	          	</div>
	          	<div class="col-md-3 teammate_desc t3">
	          		<div class="container">
	          			<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwzO7LMvyiWbP2kPhRyozXdDKplQtQTB7NlNEqPXR5s8ypic4">
	          			<span class="tag">Developpeur</span>
	          			<div class="member_name">
	          				<span class="name">Alaa Akhdeh</span>
	          			<hr>
	          		</div>
	          		</div>
	          	</div>
	          	<div class="col-md-3 teammate_desc t4">
	          		<div class="container">
	          			<img src="http://static1.squarespace.com/static/53600e19e4b0b9aa739a6514/t/560c7880e4b00a54037bb4fb/1443657856664/Anthony+DiFlorio+photo.jpg">
	          			<span class="tag">Developpeur</span>
	          			<div class="member_name">
	          				<span class="name">Steve Mbiele</span>
	          			<hr>
	          		</div>
	          		</div>
	          	</div>
	          </div>

        </body>
</html>