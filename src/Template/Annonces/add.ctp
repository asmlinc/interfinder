<!DOCTYPE html>
<html>
<head>
    <title>
        Internfinder
    </title>
    <?= $this->Html->css('home.css') ?>
    <?= $this->Html->css('contact.css') ?>
    <?= $this->Html->css('annonces.css') ?>
    <?= $this->Html->css('about.css') ?>
    <?= $this->Html->script('addAnnonce.js') ?>
</head>
<body style="background-color: #FFF; color: #000; ">

<div class="col-md-1">
</div>

<div class="col-md-10">
    <?= $this->Form->create($annonce) ?>
    <legend><?= __('Publier une annonce') ?></legend>
<fieldset class="annonce_form">

    <?php
    $this->Form->input('annonce_type', [
        'options' => ['rechercheStage' => 'Recherche de stage', 'rechercheStagiere' => 'Recherche de stagiere'], 'label' => 'Type de lannonce',
        'type' => 'select',
        'class' => 'dropdown',
        'default' => 'Recherche de stage'
    ]);

    echo $this->Form->input('stage_title', ['label' => 'Titre de l\'annonce']);
    echo $this->Form->input('stage_description', ['label' => 'Description']);
    echo $this->Form->input('qualifications_desc', ['label' => 'Qualités requises']);
    echo $this->Form->input('responsabilites_desc', ['label' => 'Responsabilités']);
    ?>

    <br>
    <br>
    <div class="Debutstage"> 
        <?php
        echo '<label id="lblDebutDate">Debut du Stage</label>';
        echo $this->Form->date('start_date_stage',[
            'year' => [
                'class' => 'year-classname',
            ],
            'month' => [
                'class' => 'month-class',
                'data-type' => 'month',
            ]
        ]);
        ?>
    </div>
    <br>
    <br>

    <div class="Finstage">
        <?php
        echo '<label id="lblFinDate">Fin du Stage</label>';
        echo $this->Form->date('end_date_stage', [
            'year' => [
                'class' => 'year-classname',
            ],
            'month' => [
                'class' => 'month-class',
                'data-type' => 'month',
            ],
        ]);
        ?>
    </div>

    <br>
    <br>
    <br>

    <?php
    if($user['user_type'] == 'etudiant') {
        echo  $this->Form->input('job_title', ['label' => 'Nom du poste']);
    }else{

        echo  $this->Form->input('program_id', [
            'options' => $programs,
            'label' => 'Nom du programme',
            'class' => 'dropdown'
        ]);
        $annonce->type_annonce = 0;

    } ?>
    <br>
    <br>
    <?php echo $this->Form->input('compensation', [
        'options' => ['0' => 'Non', '1' => 'Oui'], 'label' => 'Compensation',
        'type' => 'select',
        'class' => 'dropdown',
        'default' => '1'
    ]);
    ?>
<?= $this->Form->button(__('Enregistrer'), ['class' => 'btn btn-primary btn-lg btn-custom addUser']) ?>
<?= $this->Form->end() ?>
</fieldset>

</div>

<div class="col-md-1">
</div>

</body>
</html>