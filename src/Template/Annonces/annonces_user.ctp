<title>
        Internfinder
    </title>
    <?= $this->Html->css('annonces.css') ?>
    <?= $this->Html->css('about.css') ?>
    <?= $this->Html->script('annonce.js') ?>

    <body>
    <h1 class="title">Mes annonces </h1>
    <div id="publier_annonce">

        <button
            class="btn btn-primary btn-lg btn-custom new-msg-button"><?= $this->Html->link('Publier',array('controller' => 'annonces', 'action' => 'add'));?></button>
            </div>

          <div class="col-md-12">
              <div class="table-responsive annonce_table">
            <table class="table">
              <tr>
                  <th>Titre</th>
                  <th>Programme</th>
                  <th>Date de publication</th>
                  <th>État</th>
                  <th>Type</th>
              </tr>

              <?php foreach ($annonces as $annonce): ?>
                <tr>
                  <td>
                      <?= $this->Html->Link($annonce->stage_title, ['action' => 'view', $annonce->id_annonce]) ?>
                  </td>
                  <td>
                      <?= $this->Html->tag('span', $annonce->program->program_name, ['action' => 'view', $annonce->id_annonce]) ?>
                  </td>
                  <td>
                      <?= $this->Html->tag('span',$annonce->date_publication, ['action' => 'view', $annonce->id_annonce]) ?>
                  </td>
                  <td>
                      Ouvert
                  </td>
                  <td>
                      <?php
                          if($annonce->type_annonce == 1) { 
                            echo '<span class="glyphicon glyphicon-briefcase"></span>';
                          }
                          else if($annonce->type_annonce == 0) {
                            echo '<span class="glyphicon glyphicon-sunglasses"></span>';
                          } 
                        
                      ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          </div>
          </div>
          

            
          
  </body>