<Html>
<head>
	<?= $this->Html->css('annonces.css') ?>
	<?= $this->Html->script('annonce.js') ?>
    <?= $this->Html->css('about.css') ?>
	<?php $messageTitle = "Application à l'offre ".$annonce->stage_title." de la part de : ".$annonce->user->fullname; ?>
</head>
<body class="ls_annonces">
<!-- <div class="col-md-2">
</div> -->
<div class="col-md-12">
	<div class="contenu_page">
	<?php 
	if($this->request->session()->read('Auth.User.user_id') != $annonce->id_annonceur) {
		if($annonce->type_annonce == 1) {
			echo '<a href="#" class="btn btn-primary btn-lg btn-custom postuler" data-toggle="modal" data-target="#uploadModal">Postuler</a>';
		}
		else if($annonce->type_annonce == 0) {
			echo '<a href="#" class="btn btn-primary btn-lg btn-custom postuler" data-toggle="modal" data-target="#confirmEntrevue">Entrevue</a>';
		}
    }
    else if($this->request->session()->read('Auth.User.user_id') == $annonce->id_annonceur){
      	echo $this->Form->postLink('Supprimer', ['action' => 'delete', $annonce->id_annonce], ['class' => 'btn btn-primary btn-lg btn-custom postuler'], ['confirm' => 'Êtes-vous sûr de vouloir supprimer cette annonce ?']) ;

    }	
	?>
	
		<div class="title_annonce">
			<?= h($annonce->stage_title)?> <span>publié par <?= $this->Html->Link($annonce->user->username, ['controller' => 'users', 'action' => 'profil', $annonce->user->username]) ?> </span>
			
			<span class="glyphicon glyphicon-star-empty favorite"></span>
			<span class="added_to_fav">Ajouté au favoris!</span>
			<h5>Période du stage : <?= h($annonce->start_date_stage) ?> au <?= h($annonce->end_date_stage) ?></h5>
			<?php 
                        if($annonce->compensation == true) {
                          echo '<span class="glyphicon glyphicon-usd compens"></span style="color:#4593e3 !important; font-size: 16px;">Possibilité de rémunération</span>';
                        }
                        else {
                          echo '<span class="glyphicon glyphicon-usd no_compens"></span> <span style="color: grey !important; font-size: 16px;">Stage non rémunéré</span>';

                        }
            ?>

			<hr>
		</div>
		<div class="contenu_annonce">
		 	<section>
			 	<h4>Description:</h4> 
				<h5><?= h($annonce->stage_description) ?></h5>
			</section>

			<section>
				<h4>Qualifications:</h4> 
					<?= $annonce->qualifications_desc ?>
			</section>

			<section>
				<h4>Responsabilités:</h4>
				<?= $annonce->responsabilites_desc ?>
			</section>
		</div>
</div>
</div>
<!-- <div class="col-md-2">
</div> -->



<div id="uploadModal" class="login-block modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
				<h4 class="modal-title">Envoyer un message à <?= $annonce->user->username ?></h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('uploadFile', array( 'type' => 'file'));
				echo $this->Form->input('pdf_path', array('type' => 'file','label' => 'Pdf'));
				echo $this->Form->end();?>
			</div>
			<div class="modal-footer">
			<?= $this->Form->button(__('Envoyer')) ?>
			</div>
		</div>
	</div>
</div>

<div id="confirmEntrevue" class="login-block modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
				<h4 class="modal-title">Envoyer un message à <?= $annonce->user->username ?></h4>
			</div>
			<div class="modal-body">
				Voulez-vous vraiment programmer une entrevue avec <?= $annonce->user->username ?> ?
			</div>
			<div class="modal-footer">
			<?= $this->Form->button('Oui') ?>
			<?= $this->Form->button('Non') ?>
			</div>
		</div>
	</div>
</div>


<div id="messagingModal" class="login-block modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
				<h4 class="modal-title">Envoyer un message à <?= $annonce->user->username ?></h4>
			</div>
			<div class="modal-body">
			<?= $this->Form->create('msg',['url' =>['controller' => 'Annonces', 'action' => 'sendMessage', $annonce->user->user_id, $messageTitle]]) ?>
				    <fieldset>
				        <?php
				            echo $this->Form->input('message_coprs', array('type' => 'textarea'));
				        ?>
				    </fieldset>
				    <?php
					echo $this->Form->Submit(__('Confirmer'));
					?>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>


</body>
</Html>