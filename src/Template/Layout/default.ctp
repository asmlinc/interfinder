<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Intern Finder';
$cookie_name = 'user';
?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon') ?>
	<?= $this->Html->script('jquery-1.12.0.min.js') ?>
	<?= $this->Html->script('bootstrap.min.js') ?>
	<?= $this->Html->css('bootstrap.css') ?>

	<?= $this->Html->script('login.js') ?>

	<?= $this->Html->css('home.css') ?>
	<?= $this->Html->script('home.js') ?>


	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>

	  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
	  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	  <link rel="stylesheet" href="/resources/demos/style.css">
</head>
<body>
<div>
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar"
						aria-expanded="true" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<?php echo $this->Html->link('Acceuil', array('controller' => 'users', 'action' => 'login'), ['class' => 'navbar-brand', 'id' => 'home-button']); ?>
			</div>
			<div id="navbar" class="navbar-collapse collapse" aria-expanded="true" style="">
				<ul class="nav navbar-nav">
					<li><?php echo $this->Html->link('Offres de stages',array('controller' => 'annonces', 'action' => 'annonces'));?></li>
					<?php
							if($this->request->session()->read('Auth.User.username')) {
								echo'<li>'.$this->Html->link('Publier une annonce', array('controller' => 'annonces', 'action' => 'add')).'</li>';
							}

					?>
					<li class="active"><?php echo $this->Html->link('À propos', array('controller' => 'pages', 'action' => 'display', 'about')); ?></li>
					<li><?php echo $this->Html->link('Contact', array('controller' => 'pages', 'action' => 'display', 'contact')); ?></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<!-- <li><input type="text" class="form-control" placeholder="Search ..." name="srch-term" id="srch-term"></li> -->
					<li> <span id="userName">
							<?php 
								if($this->request->session()->read('Auth.User.username')) { 
									 echo 'Bienvenue ' .$this->request->session()->read('Auth.User.fullname');
								}
								else {
								echo $this->Html->link('S\'inscrire', array('controller' => 'users', 'action' => 'add'));
								}
							?>
						 </span>
					</li> 
					<li><?php
					if (!$this->request->session()->read('Auth.User.username')) {
							echo ' <li data-toggle="modal" data-target="#loginModal" class="icon-connect"><a class="glyphicon glyphicon-user center-align  icon-connect-font-size" aria-hidden="true"></a></li>';
					} else {
						echo ' 	<div class="btn-group">
									  <button type="button" class="btn btn-default dropdown-toggle dropdown_connect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									   <span class="glyphicon glyphicon-user center-align"></span>
									   <span class="glyphicon glyphicon-menu-down"></span>
									  </button>
									  <ul class="dropdown-menu">
									  	<li>'.$this->Html->link('Mon Profil', array('controller' => 'users', 'action' => 'profil', $this->request->session()->read('Auth.User.username'))).'</li>
									    <li>'.$this->Html->link('Mes annonces', array('controller' => 'annonces', 'action' => 'annoncesUser')).'</li>
									    <li>'.$this->Html->link('Messages', array('controller' => 'messages')).'</li>
									    <li role="separator" class="divider"></li>';
									   echo'<li>'.$this->Html->link('Déconnexion', array('controller' => 'users', 'action' => 'logout', 'class' => 'logout'), ['id' =>'btn-logout' ]) .'</li>';
									 echo' </ul>
								</div>';
					}
					?>
					</li>
				</ul>

			</div>
		</div>

	</nav>
</div>
 
<!-- MODAL POUR MOT DE PASSE OUBLIÉ -->
<div id="motDePasseOublie" class="login-block modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- MODAL CONTENT -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
				<h4 class="modal-title">Mot de passe oublié</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->input('Courriel');
				?>
				<?php
				echo $this->Form->Submit(__('Réinitialiser'), array('id' => 'button_submit'));
				?>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!-- FIN MODAL POUR MOT DE PASSE OUBLIÉ -->

<!-- MODAL -->
<div id="loginModal" class="login-block modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- MODAL CONTENT -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
				<h4 class="modal-title">Connection</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create("test", [
					'url' => ['controller' => 'Users', 'action' => 'login']]); ?>
				<!--        <h1>Connection</h1>-->
				<?php echo $this->Form->input('username');
				echo $this->Form->input('password', array('type' => 'password'));
				?>
				<?php
				echo $this->Form->Submit(__('Confirmer'), array('id' => 'button_submit'));
				?>
				<div id="new-to-internfinder"><a href="../Users/add">Je ne possède pas de compte</a></div>
				<div id="forgotten_password"><a href="#" data-toggle="modal" data-dismiss="modal" data-target="#motDePasseOublie">Mot de passe oublié</a></div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>
<?= $this->Flash->render() ?>
<section class="container clearfix">
	<?= $this->fetch('content') ?>
</section>

</body>
</html>
