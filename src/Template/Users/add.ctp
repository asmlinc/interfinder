<?= $this->Html->css('add.css') ?>
<?= $this->Html->script('add.js') ?>
<?= $this->Html->css('about.css') ?>

<body>
<div id="title">Créer un compte</div>

<hr>
<div class="users form">
    <div class="col-md-2">
    </div>
    <div class="col-md-8 form info-form">
    <!-- <span class="error_message">Les mots de passe ne correspondent pas. Veuillez réessayer!</span> -->
        <?= $this->Form->create($user) ?>
        <fieldset class= "commForm">
            <legend><?= __('Informations générales') ?></legend>
            <?= $this->Form->input('username', ['label' => 'Nom d\'utilisateur']) ?>
            <?= $this->Form->input('password', ['label' => 'Mot de passe', 'type' => 'password', 'class' => 'password1']) ?>
            <?= $this->Form->input('password', ['label' => 'Confirmation du mot de passe', 'type' => 'password', 'class' => 'password2']) ?>
            <?= $this->Form->input('fullname', ['label' => 'Nom complet']) ?>
            <?= $this->Form->input('mail', ['label' => 'Courriel', 'class' => 'email']) ?>
            <?= $this->Form->input('phone_number', ['label' => 'Numéro de téléphone']) ?>
            <?= $this->Form->input('user_type', [
                'options' => ['etudiant' => 'Étudiant', 'entreprise' => 'Entreprise', 'corps_enseignant' => 'Corps enseignant'], 'label' => 'Type de compte',
                'type' => 'select',
                'class' => 'dropdown',
                'default' => 'Étudiant'
            ]) ?>
        </fieldset>
    <div id="second_part">
        <div class="student">
            <fieldset id="studentField">
                <legend><?= __('Informations élèves') ?></legend>
                <?= $this->Form->input('da_student', ['label' => 'Numéro D.A']) ?>
                <?= $this->Form->input('age') ?>
                <?= $this->Form->input('college_id', [
                    'options' => $college,
                    'label' => 'Nom de l\'établissement',
                    'class' => 'dropdown'
                ]) ?>
                <?= $this->Form->input('program_id', [
                    'options' => $programs,
                    'label' => 'Nom du programme',
                    'class' => 'dropdown'
                ]) ?>
            </fieldset>
        </div>

        <div class="teacher">
            <fieldset class="teacherField">
                <legend><?= __('Informations professeurs') ?></legend>
                <?= $this->Form->input('teacher_college_id', [
                    'options' => $college,
                    'label' => 'College',
                    'class' => 'dropdown'
                ]) ?>
            </fieldset>
        </div>

        <div class="company">
            <fieldset class="companyField">
                <legend><?= __('Informations compagnie') ?></legend>
                <?= $this->Form->input('company_name', ['label' => 'Nom de la compagnie']) ?>
                <?= $this->Form->input('company_address', ['label' => 'Adresse:']) ?>
            </fieldset>
        </div>
    </div>

        <?= $this->Form->button(__('Ajouter'), ['class' => 'btn btn-primary btn-lg btn-custom addUser']); ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->button(__('Étape 1'), ['class' => 'btn btn-primary btn-lg btn-custom prev_step ']); ?>
        <?= $this->Form->button(__('Étape 2'), ['class' => 'btn btn-primary btn-lg btn-custom next_step ']); ?>
    </div>


    <div class="col-md-2">
    </div>
</div>
</body>
</html>


