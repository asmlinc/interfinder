<html>
<head>
	<?= $this->Html->css('profil.css'); ?>
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

<body>

<div class="col-md-3">
	<div class="user_panel">
		<div class="desc_title">Profil 
		<span class="glyphicon glyphicon-pencil" aria-hidden="true"><?= $this->Html->link('Modifier', array('controller' => 'users', 'action' => 'editProfile')); ?></span></div>
		<div class="desc_user">
			<span class="title"><?php echo $temp->fullname ?></span>
			<h5 class="username">@<?php echo $temp->username ?></h5>

			<img id="img_profil" src="http://www.lcfc.com/images/common/bg_player_profile_default_big.png">

			<!-- <h5> Courriel: <?php $temp->mail ?></h5> -->
			<?php 
			if($temp->user_type == 'etudiant') {
				echo 'Étudiant(e), '. $info->age . ' ans';
			}
			else if($temp->user_type == 'entreprise') {
				echo 'Entreprise';
			}
			?>		

<!--			<h5 style="margin-top: 20px;">-->
<!--			--><?php //
//					if($temp->description == null) {
//						echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
//					}
//					else {
//						echo $temp->description;
//					}
//				?>
<!--				</h5>-->
		</div>
				<button class="btn btn-primary btn-lg btn-custom" data-toggle="modal" data-target="#messagingModal">Contacter</button>
		        <!-- <?= $this->Form->button(__('Contacter'), ['class' => 'btn btn-primary btn-lg btn-custom ']); ?> -->

	</div>
</div>

<div class="col-md-9">
	<!-- <div class="row_desc"> -->
	<div class="row">
		<div class="col-md-6">
			<div class="desc_title">
				<?php
					if($temp->user_type == 'etudiant') {
						echo 'Cheminement scolaire';
					}
					else {
						echo 'À propos de l\'entreprise';
					}
				?>
			</div>
			<h5 class="description">
			<?php
				if($temp->user_type == 'etudiant') {
					if($info->cursus_aca == null) {
						echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
					}
					else {
						echo '<pre>'.$info->cursus_aca.'</pre>';
					}
				}
				else {
					if($info->description == null) {
						echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
					}
					else {
						echo '<pre>'.$info->description.'</pre>';
					}
				}

			?>
			</h5>
		</div>
		<div class="col-md-6">
			<div class="desc_title">
				<?php
					if($temp->user_type == 'etudiant') {
						echo 'Qualités';
					}
					else {
						echo 'Notre mission';
					}
				?>
			</div>
			<h5 class="description">
			<?php
				if($temp->user_type == 'etudiant') {
					if($info->qualites == null) {
						echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
					}
					else {
						echo  '<pre>'.$info->qualites.'</pre>';
					}
				}
				else {
					if($info->mission == null) {
						echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
					}
					else {
						echo $info->mission;
					}
				}
			?>
			</h5>

		</div>
	</div>


<!-- 	<div class="row">
		<div class="row_desc">
		<div class="desc_title">Description</div>
			<h5 class="description">
				<?php
					if($info->description == null) {
						echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
					}
					else {
						echo  '<pre>'.$info->description.'</pre>';
					}
				?>
			</h5>
		</div>
	</div> -->
	<div class="row">
		<div class="row_desc">
		<div class="desc_title">

			<?php
					if($temp->user_type == 'etudiant') {
						echo 'Expériences de travail';
					}
					else {
						echo 'Notre location';
					}
				?>
		</div>
			<h5 class="description">
				<?php
					if($temp->user_type == 'etudiant') {
						if($info->experiences == null) {
							echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
						}
						else {
							echo $info->experiences;
						}
					}
					else {
						if($info->company_address == null) {
							echo 'Nous nous situons au:<br>';
							echo 'Il n\'y a aucune information enregistrée  à ce sujet pour cet utilisateur';
						}
						else {
							echo $info->company_address;
						}
					}
					?>
			</h5>
		</div>
	</div>
	<!-- <div class="row_desc"> -->

</div>

</div>

<div id="messagingModal" class="login-block modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
				<h4 class="modal-title">Envoyer un message à <?= $temp->fullname ?></h4>
			</div>
			<div class="modal-body">
			<?= $this->Form->create('msg',['url' =>['controller' => 'Users', 'action' => 'sendMessage', $temp->user_id]]) ?>
				    <fieldset>
				        <?php
				        	echo $this->Form->input('titre_message', array('label' => '', 'placeholder' => 'Entrez le titre du message'));
				            echo $this->Form->input('message_corps', array('type' => 'textarea', 'label' => '', 'placeholder' => 'Entrez le corps du message'));
				        ?>
				    </fieldset>
				    <?php
					echo $this->Form->Submit(__('Confirmer'));
					?>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>  

</body>
</head>
</html>