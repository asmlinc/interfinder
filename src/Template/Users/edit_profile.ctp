<html>
<head>
    <?= $this->Html->css('bootstrap.min.css'); ?>
    <?= $this->Html->css('profiledit.css'); ?>
    <?= $this->Html->script('jquery-1.12.0.min.js'); ?>
    <?= $this->Html->script('bootstrap.min.js'); ?>
    <?= $this->Html->script('profileEditScripts.js'); ?>
<body>

<?= $this->Form->create($userc) ?>
<div class="row text-center">
    <h2 class="text-center">Modifier compte</h2>
</div>
<fieldset >

	<?= $this->Form->input('username', ['label' => 'Nom d\'utilisateur']) ?>
	<?= $this->Form->input('password', ['label' => 'Mot de passe', 'type' => 'password']) ?>
	<?= $this->Form->input('fullname', ['label' => 'Nom complet']) ?>
	<?= $this->Form->input('mail', ['label' => 'Courriel']) ?>
	<?= $this->Form->input('phone_number', ['label' => 'Numéro de téléphone']) ?>
	<?= $this->Form->input('description', ['label' => 'Description']) ?>

</fieldset>

<div class="row col-sm-12 text-center transparent">
    <h2></h2>
    <?= $this->Form->button(__('Enregistrer'), ['class' => 'btn btn-primary btn-lg btn-custom addUser']); ?>
    <?= $this->Form->end() ?>
</div>


</body>
</head>
</html>