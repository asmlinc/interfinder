<!DOCTYPE html>
<html>
  <head>
    <title>
    Internfinder
    </title>
    <?= $this->Html->css('home.css') ?>
  </head>
  <body> 
  <span id="menu">
      <ul>
        <li><a class="active" href="#home">Accueil</a></li>
        <li><a href="#stagesOfferts">Stages offerts</a></li>
        <li><a href="#about">À propos</a></li>
        <li><a href="#about">Contact</a></li>
        <li class="icon-search"><a class="glyphicon glyphicon-search" aria-hidden="true"></a></li>
        <li data-toggle="modal" data-target="#loginModal" class="icon-connect"><a class="glyphicon glyphicon-user" aria-hidden="true"></a></li>
      </ul>
    </span>

    </div>
    

<!-- MODAL -->
<div id="loginModal" class="login-block modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- MODAL CONTENT -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="btn_close_modal">&times;</button>
        <h4 class="modal-title">Connection</h4>
      </div>
      <div class="modal-body">
      <?= $this->Flash->render('auth') ?>
      <?php echo $this->Form->create(); ?>
        <!--        <h1>Connection</h1>-->
        <?php echo $this->Form->input('username');
        echo $this->Form->input('password', array('type' => 'password'));
        echo $this->Form->input('user_type', array(
          'options' => array('étudiant' => 'Étudiant', 'entreprise' => 'Entreprise', 'corps enseignant' => 'Corps enseignant')
        ));
        ?>
        <?= $this->Form->Submit(__('Submit'), array('id' => 'button_submit')); ?>
        <div id="new-to-internfinder"><a href="#">Je ne possède pas de compte</a></div>
        <div id="forgotten_password"><a href="#">Mot de passe oublié</a></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<?php echo $this->Form->end(); ?>


  <div class="col-md-3">
  </div>

  <div class="col-md-6">
  <h1 class="page_title">Internfinder </h1>
  <h3>Find your next internship in one click!</h3>
  <div class="buttons">
  <a href="../Users/login" class="btn btn-primary btn-lg btn-custom ">Se connecter</a>
  <a href="#" class="btn btn-primary btn-lg btn-custom ">Découvrir</a>
  </div>
  </div>

  <div class="col-md-3">
  </div>

  </body>
</html>