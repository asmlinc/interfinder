<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController {
	var $uses = array('Users');

	public function initialize() {
		parent::initialize();
		/*$this->Auth->config('authenticate', [
				'Basic' => ['userModel' => 'Users'],
				'Form' => ['userModel' => 'Users'],
			]);

			/*	$this->loadComponent('Auth', [
				'loginAction' => [
					'controller' => 'Home',
					'action' => 'login',
		*/

		//debug($this->Auth);

		//$this->set('users', $this->Users->find('all'));
		$this->loadModel('Users');
		$this->log('Got here in initialize HomeController', 'debug');
	}

	public function index() {
		$this->set('users', $this->Users->find('all'));
		//$this->loadModel('Users');
		$this->log('Got here in index HomeController', 'debug');
	}

	public function login() {
		//debug($uses);
		$this->loadModel('Users');
		//$uses = $this->User->find('all');

		$this->log('Got in login function inside HomeController', 'debug');
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			$this->log('In login function, user is: ', 'debug');
			debug($user);
			if ($user) {
				$this->log('Got here', 'debug');
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__("Nom d'utilisateur ou mot de passe incorrect. Essayez à nouveau."));
		}
	}

	public function view($id) {
		$this->loadModel('Users');
		$user = $this->Users->get($id);
		$this->set(compact('user'));
	}

	/**
	 * Displays a view
	 *
	 * @return void|\Cake\Network\Response
	 * @throws \Cake\Network\Exception\NotFoundException When the view file could not
	 *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
	 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		$this->set(compact('page', 'subpage'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingTemplateException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
}
