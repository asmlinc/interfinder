<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Annonces Controller
 *
 * @property \App\Model\Table\AnnoncesTable $Annonces
 */
class AnnoncesController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $creerAnnonceURL = array(
            'controller' => 'Annonces',
            'action' => 'display',
            'creerAnnonce'
        );
        $this->Auth->allow(['annonces', 'view']);
        $this->set('creerAnnonceURL', $creerAnnonceURL);
        $this->set('user', $this->request->session()->read('Auth.User'));

    }

    public function index()
    {
        $this->set('annonces', $this->Annonces->find('all'));
    }

    public function annoncesUser()
    {
        $uid = $this->Auth->user('user_id');
        $this->set('annonces', $this->Annonces->find('all')->where(['Annonces.id_annonceur' => $uid])->contain(['Users'])->contain(['Programs']));
        // $query->where(['Annonces.id_annonceur' => $uid]);
        //$this->set(compact('annonces'));
    }

    public function annonces()
    {
        $this->set('array_received', $this->request->session()->read('arrayIds'));
        if (is_null($this->request->session()->read('arrayIds'))) {
            $this->log('NOOOOOOOOO', 'debug');
            $this->set('annonces', $this->Annonces->find('all', array('order' => array('date_publication' => 'asc')))->contain(['Users'])->contain(['Programs']));
            $this->set(compact('annonces'));
        } else {
            $this->log('ici', 'debug');
            $this->set('annonces', $this->Annonces->find('all', array('order' => array('date_publication' => 'asc')))->contain(['Users'])->contain(['Programs'])->where(['program_id IN' => $this->request->session()->read('arrayIds')]));
            $this->set(compact('annonces'));
            $this->request->session()->delete('arrayIds');
        }

        // $nombreAnnonces = sizeof($annonces);
    }

    public function view($id = null)
    {
        $annonce = $this->Annonces->get($id, [
            'contain' => ['Users']
        ]);
        $this->set(compact('annonce'));
    }

    public function isAuthorized($user)
    {

        // Par défaut refuser
        return true;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $this->setUpDropdowns();

        $annonce = $this->Annonces->newEntity();

        if ($this->request->session()->read('Auth.User.user_type') === 'etudiant') {
            $annonce['type_annonce'] = 0;
            $this->loadModel('Students');
            $info = $this->Students->find('all')->where(['user_id' => $this->request->session()->read('Auth.User.user_id')])->first();
            $this->set('info', $info);
        } else {
            $annonce['type_annonce'] = 1;
        }

        if ($this->request->is('post')) {
            $annonce = $this->Annonces->patchEntity($annonce, $this->request->data);
            $annonce->id_annonceur = $this->request->session()->read('Auth.User.user_id');
            $annonce->date_publication = date("y-m-d");
            if ($this->request->session()->read('Auth.User.user_type') === 'etudiant') {
                $annonce['program_id'] = $info->program_id;
            } else {
                $annonce['program_id'] = $annonce['program_id'] + 1;
            }
            if ($this->Annonces->save($annonce)) {
                $this->Flash->success(__('Annonce enregistrée'));
                return $this->redirect(['action' => 'annonces']);
            } else {
                $this->Flash->error(__('Une erreure est survenue lors de la sauvegarde cette de l\'annonce.'));
            }
        }
        $this->set(compact('annonce', 'users'));
        $this->set('_serialize', ['annonce']);
    }

    private function setUpDropdowns()
    {
        $programs = $this->getPrograms();
        $formatedPrograms = $this->formatDropdownPrograms($programs);

        $this->set('programs', $formatedPrograms);
    }

    public function getPrograms()
    {
        $conn = ConnectionManager::get('default');
        $querry = $conn->prepare(
            'SELECT * FROM `programs` ORDER BY `programs`.`id_program` ASC'
        );
        $querry->execute();
        $liste = array();

        do {
            $progs = $querry->fetch('assoc');
            array_push($liste, $progs);
        } while ($progs);

        return $liste;
    }

    public function formatDropdownPrograms($liste)
    {

        $formatedList = array();
        $cmp = 0;
        foreach ($liste as $ele) {
            if ($ele) {
                array_push($formatedList, ($ele['id_program']));
                $formatedList[$cmp] = $ele['program_name'];
                $cmp++;
            }
        }
        return $formatedList;
    }

    /**
     * Edit method
     *
     * @param string|null $id Annonce id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $annonce = $this->Annonces->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $annonce = $this->Annonces->patchEntity($annonce, $this->request->data);
            if ($this->Annonces->save($annonce)) {
                $this->Flash->success(__('Annonce sauvegargée'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Une erreure est survenue lors de l\'édition cette de l\'annonce.'));
            }
        }
        $programmes = $this->Annonces->Programmes->find('list', ['limit' => 200]);
        $this->set(compact('annonce', 'programmes'));
        $this->set('_serialize', ['annonce']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Annonce id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $annonce = $this->Annonces->get($id);
        if ($this->Annonces->delete($annonce)) {
            $this->Flash->success(__('Annonce supprimée'));
        } else {
            $this->Flash->error(__('Une erreure est survenue lors de la supression de l\'annonce.'));
        }
        return $this->redirect(['action' => 'annonces']);
    }

    public function sendMessage($id_destinataire = null, $messageTitle = null)
    {
        $messageController = new MessagesController();

        $message = $messageController->Messages->newEntity();
        $message = $messageController->Messages->patchEntity($message, $this->request->data);

        $messageCoprs = $message->message_coprs;
        $messageController->add($messageCoprs, $id_destinataire, $messageTitle);

        $this->redirect(
            ['action' => 'annonces']
        );
    }
}
