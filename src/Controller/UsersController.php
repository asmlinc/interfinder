<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\UsersTable;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Connection;
use Cake\Log\Log;

class UsersController extends AppController
{
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['add', 'logout', 'rechercherStage']);
		$this->set('user', $this->Auth->User());
	}

	public function add()
	{
		$this->setUpDropdowns();
		$user = $this->Users->newEntity();

		$StudentsController = new StudentsController();
		$TeachersController = new TeachersController();
		$CompaniesController = new CompaniesController();


		if ($this->request->is('post')) {
			$this->log('Passed post in add function', 'debug');

			$user = $this->Users->patchEntity($user, $this->request->data);

			if ($this->Users->save($user)) {
				$this->Flash->success(__("L'utilisateur a été sauvegardé."));
			}
			if (strcmp($user['user_type'], 'etudiant') == 0) {
				$this->prepareStudent($StudentsController, $user, $student);
				if ($StudentsController->Students->save($student)) {
					return $this->redirect(['action' => 'login']);
				}
			}
//
			if (strcmp($user['user_type'], 'entreprise') == 0) {
				$this->prepareCompany($CompaniesController, $user, $company);

				if ($CompaniesController->Companies->save($company)) {
					return $this->redirect(['action' => 'login']);
				}
			}
//
			if (strcmp($user['user_type'], 'corps_enseignant') == 0) {
				$this->prepareTeacher($TeachersController, $user, $teacher);

				if ($TeachersController->Teachers->save($teacher)) {
					return $this->redirect(['action' => 'login']);
				}
			}
		}
		$this->set('user', $user);
	}

	public function login()
	{
		$this->log('Got in login function inside ', 'debug');
		if ($this->request->is('post') || $this->request->is('put')) {
			$user = $this->Auth->identify();
			$this->log('In login function, user is: ', 'debug');
			if ($this->Auth->identify()) {
				$this->log('Got here', 'debug');
				$this->Auth->setUser($user);
				$this->set('user', $user);


				if($this->request->session()->read('Auth.User.username')) {
					$this->Flash->success("Bienvenue ". $this->request->session()->read('Auth.User.fullname'));
				}
				return $this->redirect(['Controller' => 'users', 'action' => 'login']);
			}
			$this->Flash->error(__("Nom d'utilisateur ou mot de passe incorrect. Essayez à nouveau."));
		}

	}

	public function logout()
	{
		$this->Flash->success('Aurevoir, '.$this->request->session()->read('Auth.User.fullname'));
		$this->redirect($this->Auth->logout());
	}

	public function index()
	{
		$this->set('users', $this->Users->find('all'));
	}

	public function view($id)
	{
		$user = $this->Users->get($id);
		$this->set(compact('user'));
	}

	public function profil($username)
	{
		$temp = $this->Users->find()->where(['username' => $username])->first();
		$this->set('temp', $temp);

		if($temp->user_type == 'etudiant') {
			$this->log('Cet utilisateur est un étudiant', 'debug');
					$this->loadModel('Students');
					$uid = $temp->user_id;
			$this->log('le id est '.$uid, 'debug');
					$this->set('info', $this->Students->find()->where(['Students.user_id' => $uid])->first()); 
					// $this->set(compact('info'));
		}
		if($temp->user_type == 'entreprise') {
			$this->log('Cet utilisateur est une compagnie', 'debug');
					$this->loadModel('Companies');
					$uid = $temp->user_id;
					$this->log($uid, 'debug');
					$this->set('info', $this->Companies->find()->where(['Companies.user_id' => $uid])->first()); 
		}
	}

	public function UploadComponents($data = null)
	{
		if (!empty($data)) {
			if (count($data) > 3) {
				throw new NotFoundException("Error processing Request, Max number files accepted is 1", 1);
			}
			foreach ($data as $file) {
				$filename = $file['name'];
				$file_tmp_name = $file['tmp_name'];
				$dir = WWW_ROOT . 'img' . DS . 'uploads';
				if (is_uploaded_file($file_tmp_name)) {
					move_uploaded_file($file_tmp_name, $dir . DS . String::uuid() . '-');
				}
			}
		}
	}

	/**
	 *
	 */

	public function editProfile()
	{
		$userTemp = $this->Users->newEntity();
		$id = $this->request->session()->read('Auth.User.user_id');
		$userc = $this->Users->get($id, [
			'contain' => []
		]);

		$this->set('username', $userc->username);
		$this->set('fullname', $userc->fullname);
		$this->set('description', $userc->description);
		$this->set('email', $userc->mail);
		$this->set('phone', $userc->phone_number);

		if ($this->request->is(['patch', 'post', 'put'])) {
			$userc = $this->Users->patchEntity($userc, $this->request->data);
			if ($this->Users->save($userc)) {
				$this->Flash->success(__("L'utilisateur a été sauvegardé."));
				return $this->redirect(['action' => 'profil', $this->request->session()->read('Auth.User.username') ]);
			}else {
				$this->Flash->error(__('Les changement n\'ont pas pu etre sauvegarder, S.V.P reessayer'));
			}
		}

		$this->set(compact('userc'));
		$this->set('_serialize', ['userc']);
	}

	public function isAuthorized($user)
	{
		// Admin peuvent accéder à chaque action
		if (isset($user['user_type'])) {
			return true;
		}

		// Par défaut refuser
		return false;
	}

	public function getColleges()
	{
		$conn = ConnectionManager::get('default');
		$querry = $conn->prepare(
			'SELECT * FROM `colleges` ORDER BY `colleges`.`id_colleges` ASC'
		);
		$querry->execute();
		$liste = array();

		do {
			$coll = $querry->fetch('assoc');
			array_push($liste, $coll);
		} while ($coll);

		return $liste;
	}

	public function getUser($id)
	{
		$conn = ConnectionManager::get('default');
		$querry = $conn->prepare(
			"SELECT * FROM users where user_id = $id "
		);
		$querry->execute();
		return $querry->fetch('assoc');
	}

	public function formatDropdowCollege($liste)
	{

		$formatedList = array();
		$cmp = 0;
		foreach ($liste as $ele) {
			if ($ele) {
				array_push($formatedList, ($ele['id_colleges']));
				$formatedList[$cmp] = $ele['college_name'];
				$cmp++;
			}
		}
		return $formatedList;
	}

	public function getPrograms()
	{
		$conn = ConnectionManager::get('default');
		$querry = $conn->prepare(
			'SELECT * FROM `programs` ORDER BY `programs`.`id_program` ASC'
		);
		$querry->execute();
		$liste = array();

		do {
			$progs = $querry->fetch('assoc');
			array_push($liste, $progs);
		} while ($progs);

		return $liste;
	}

	public function formatDropdowPrograms($liste)
	{

		$formatedList = array();
		$cmp = 0;
		foreach ($liste as $ele) {
			if ($ele) {
				array_push($formatedList, ($ele['id_program']));
				$formatedList[$cmp] = $ele['program_name'];
				$cmp++;
			}
		}
		return $formatedList;
	}

	private function setUpDropdowns()
	{
		$college = $this->getColleges();
		$formatedColleges = $this->formatDropdowCollege($college);
		$programs = $this->getPrograms();
		$formatedPrograms = $this->formatDropdowPrograms($programs);

		$this->set('college', $formatedColleges);
		$this->set('programs', $formatedPrograms);
	}

	/**
	 * @param $StudentsController
	 * @param $user
	 * @param $student
	 */
	public function prepareStudent($StudentsController, $user, &$student)
	{
		$student = $StudentsController->Students->newEntity();
		$student['user_id'] = $user['user_id'];
		$student['college_id'] = $user['college_id'] + 1;
		$student['program_id'] = $user['program_id'] + 1;
		$student['da_student'] = $user['da_student'];
		$student['first_name'] = $user['first_name'];
		$student['last_name'] = $user['last_name'];
		$student['age'] = $user['age'];
	}

	/**
	 * @param $CompaniesController
	 * @param $user
	 * @param $company
	 */
	public function prepareCompany($CompaniesController, $user, &$company)
	{
		$company = $CompaniesController->Companies->newEntity();
		$company['user_id'] = $user['user_id'];
		$company['company_address'] = $user['company_address'];
		$company['company_name'] = $user['company_name'];
	}

	/**
	 * @param $TeachersController
	 * @param $user
	 * @param $teacher
	 */
	public function prepareTeacher($TeachersController, $user, &$teacher)
	{
		$teacher = $TeachersController->Teachers->newEntity();
		$teacher['user_id'] = $user['user_id'];
		$teacher['teacher_college_id'] = $user['teacher_college_id'] + 1;
		$teacher['teacher_first_name'] = $user['teacher_first_name'];
		$teacher['teacher_last_name'] = $user['teacher_last_name'];
	}

	public function rechercherStage()
	{
		$elem = $this->request->data('srch-term');
		$this->loadModel('Programs');
		$this->loadModel('Annonces');
		$results = $this->Programs->find('all')->where(['program_name LIKE' => '%'.$elem.'%']);
		$nbResult = $results->select(['count' =>$results->func()->count('*')]);
		$this->set('all_results',$this->Programs->find('all')->where(['program_name LIKE' => '%'.$elem.'%']));

		$this->set('count',$nbResult->first()->count );
		if ($nbResult->first()->count == 0){
			// redirect vers  la page not found
		} else {
			$arrayIds = [];
			foreach ($this->Programs->find('all')->where(['program_name LIKE' => '%'.$elem.'%']) as $result){
				array_push($arrayIds, $result->id_program);
			}

			$session = $this->request->session();
			$session->write('arrayIds', $arrayIds);
			$this->set('array_ids', $arrayIds);
			$hello = 'Hello';
			$this->set('Hello', $hello);
//			 $this->redirect(array('controller' => 'Annonces', 'action' => 'annonces'));

		}

	}

	public function sendMessage($id_destinataire = null)
	{
		$messageController = new MessagesController();

		$message = $messageController->Messages->newEntity();
		$message = $messageController->Messages->patchEntity($message, $this->request->data);

		$messageCorps = $message->message_corps;
		$messageTitle = $message->titre_message;
		if ($messageCorps != null && $messageTitle != null) {
			$this->set('msg', $messageCorps);


			$messageController->add($messageCorps, $id_destinataire, $messageTitle);
		}
			$this->redirect(
				['action' => 'profil', $this->request->session()->read('Auth.User.username')]
			);

	}
}

?>
