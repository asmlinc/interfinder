<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages */
class MessagesController extends AppController
{

    public function isAuthorized($user)
    {
        // Admin peuvent accéder à chaque action
        if (isset($user['user_type'])) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $userController = new UsersController();

        $this->paginate = [
            'contain' => ['Users']
        ];
        $messages = $this->paginate($this->Messages);

        $query = $this->Messages->find('all')->where(['id_expediteur' => $this->request->session()->read('Auth.User.user_id')]);
        $query2 = $this->Messages->find('all')->where(['id_destinataire' => $this->request->session()->read('Auth.User.user_id')]);
        $results = $query->toArray();
        $results2 = $query2->toArray();
        $messages =array_merge($results, $results2);
        foreach ($messages as $message){
            $senderQuery = $userController->Users->find('all')->where(['user_id' => $message->id_expediteur]);
            $receiverQuery = $userController->Users->find('all')->where(['user_id' => $message->id_destinataire]);
            $sender= $senderQuery->toArray();
            $receiver= $receiverQuery->toArray();
            $message->sender = $sender;
            $message->receiver = $receiver;
        }
//        print_r($messages);
        $this->set(compact('messages'));
        $this->set('_serialize', ['messages']);
    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userController = new UsersController();
        $message = $this->Messages->get($id, [
            'contain' => ['Users']
        ]);
        $ogMessage = $message;
        $senderQuery = $userController->Users->find('all')->where(['user_id' => $message->id_expediteur]);
        $receiverQuery = $userController->Users->find('all')->where(['user_id' => $message->id_destinataire]);
        $sender= $senderQuery->toArray();
        $receiver= $receiverQuery->toArray();
        $message->sender = $sender;
        $message->receiver = $receiver;
        if ($ogMessage->isRead == false){
            $ogMessage->isRead = true;
            $this->Messages->save($ogMessage);
        }

//        print_r($test);
        $this->set('message', $message);
        $this->set('_serialize', ['message']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($messageCorps= null, $id_destinataire = null, $messageTitle = null)
    {


        $this->set('msg',$messageCorps);
        if($id_destinataire === null){
            $message = $this->Messages->newEntity();
            if ($this->request->is('post')) {
                $message = $this->Messages->patchEntity($message, $this->request->data);
                $message->id_expediteur = $this->request->session()->read('Auth.User.user_id');
                $message->date_message = date("Y-m-d");
                $message->isRead = 0;
                $message->id_destinataire = $message->id_destinataire +1;
                if ($this->Messages->save($message)) {
                    $this->Flash->success(__('Le message à été envoyé.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Ce message n\'a pas pu etre delivré. S.V.P recommencer' ));
                }
            }

        }else{
            $message = $this->Messages->newEntity();
            $message->id_expediteur = $this->request->session()->read('Auth.User.user_id');
            $message->date_message = date("Y-m-d");
            $message->isRead = 0;
            $message->id_destinataire = $id_destinataire ;
            $message->titre_message = $messageTitle;
            $message->corps_message = $messageCorps;
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('Le message à été envoyé.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Ce message n\'a pas pu etre delivré. S.V.P recommencer' ));
            }
        }


         $usersQuerry = $this->Messages->Users->find();
         $users = $this->formatUserList($usersQuerry);
         $this->set(compact('message', 'users'));
         $this->set('_serialize', ['message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $message = $this->Messages->patchEntity($message, $this->request->data);
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The message could not be saved. Please, try again.'));
            }
        }
        $users = $this->Messages->Users->find('list', ['limit' => 200]);
        $this->set(compact('message', 'users'));
        $this->set('_serialize', ['message']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('Message supprimer.'));
        } else {
            $this->Flash->error(__('Une erreure est survenue lors de la supression du message.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function formatUserList($liste){
        $formatedList = array();

        $cmp = 0;
        foreach($liste as $ele){

            array_push($formatedList, ($ele->user_id));
            $formatedList[$cmp]= $ele->username;
            $cmp++;
        }
        return $formatedList;
    }
}
